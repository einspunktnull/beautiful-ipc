import "source-map-support/register";
import * as child_process from "child_process";

(async () => {
    console.log("example startington");

    child_process.fork("./dist/example/run-server.js");

    for (let i = 1; i <= 5; i++) {
        setTimeout(() => {
            child_process.fork("./dist/example/run-client.js", ["nr-" + i]);
        }, i * 2000);
    }
})();
