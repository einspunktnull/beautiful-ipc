import "source-map-support/register";
import { Client } from "../lib/Client";
import { CLIENT_ID, SERVER_ID } from "./constants";
import { Config } from "../lib/common";

(async () => {
    const config: Config = { debug: true };
    const suffix: string = process.argv[2];
    const client: Client = await Client.start(SERVER_ID, CLIENT_ID + "-" + suffix, config);
    await client.stop();
    // await client.start();
    // setTimeout(async ()=>{
    //     await client.stop();
    // },5000)
})();
