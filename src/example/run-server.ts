import "source-map-support/register";
import { SERVER_ID } from "./constants";
import { Server } from "../lib/Server";
import { Config } from "../lib/common";

(async () => {
    const config: Config = { debug: true };
    const server: Server = await Server.start(SERVER_ID, config);
    setTimeout(async () => {
        await server.stop();
        // process.exit(0);
    }, 30000);
})();
