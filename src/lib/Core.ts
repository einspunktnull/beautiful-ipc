import {
    Config,
    CustomEventName,
    DefaultConfig,
    ERROR_INSTANCE_LOCKED,
    ERROR_STATE_IS_NOT_STARTED,
    ERROR_STATE_IS_NOT_STOPPED,
    EventName,
    NodeIpcClientOrServer,
    NodeIpcStatic,
    RejectFn,
    ResolveFn,
    State,
} from "./common";
import assert from "assert";
import { Socket } from "net";
import { Logger } from "./Logger";

export abstract class Core<NodeIpcInstance extends NodeIpcClientOrServer> {
    private static INSTANCE_LOCKED: boolean = false;

    protected readonly logger: Logger;

    private _resolve: ResolveFn;
    private _reject: ResolveFn;

    private _nodeIpcInstance: NodeIpcInstance;
    private _state: State = State.STOPPED;
    private readonly _config: Config;

    protected get config(): Config {
        return this._config;
    }

    protected get nodeIpcInstance(): NodeIpcInstance {
        return this._nodeIpcInstance;
    }

    protected get state(): State {
        return this._state;
    }

    public get id(): string {
        return NodeIpcStatic.config.id;
    }

    protected abstract initialize(): void;

    protected abstract terminate(): void;

    protected constructor(id: string, config?: Config) {
        this._config = {
            ...DefaultConfig,
            ...config,
        };
        this.logger = new Logger(this._config.debug);
        assert(!Core.INSTANCE_LOCKED, ERROR_INSTANCE_LOCKED);
        Core.INSTANCE_LOCKED = true;
        NodeIpcStatic.config = {
            ...NodeIpcStatic.config,
            id: id,
            maxRetries: 0,
            silent: true,
        };
    }

    public async start(): Promise<this> {
        this.assertStateStopped();
        this.setState(State.STARTING);
        await this.run(this.initialize.bind(this));
        this.setState(State.STARTED);
        return this;
    }

    public async stop(): Promise<this> {
        this.assertStateStarted();
        this.setState(State.STOPPING);
        await this.run(this.terminate.bind(this));
        this.setState(State.STOPPED);
        return this;
    }

    protected resolve(): void {
        if (!this._resolve) return this.onUnknownError();
        this._reject = undefined;
        const resolve: ResolveFn = this._resolve;
        this._resolve = undefined;
        resolve();
    }

    protected reject(err: Error): void {
        if (!this._reject) return this.onUnknownError(err);
        this._resolve = undefined;
        const reject: RejectFn = this._reject;
        this._reject = undefined;
        reject(err);
    }

    protected assertStateStarted(): void {
        assert(this.state === State.STARTED, `${ERROR_STATE_IS_NOT_STARTED}: ${this.id}`);
    }

    protected assertStateStopped(): void {
        assert(this.state === State.STOPPED, `${ERROR_STATE_IS_NOT_STOPPED}: ${this.id}`);
    }

    protected setState(value: State): void {
        this._state = value;
    }

    protected initializeNodeIpcInstance(nodeIpcInstance: NodeIpcInstance) {
        if (!!this.nodeIpcInstance) this.unregisterEventListeners();
        this._nodeIpcInstance = nodeIpcInstance;
        this.registerEventListeners();
    }

    protected finalizeNodeIpcInstance() {
        assert(!!this.nodeIpcInstance);
        this.unregisterEventListeners();
        this._nodeIpcInstance = undefined;
    }

    protected registerEventListeners(): void {
        this.nodeIpcInstance.on(EventName.ERROR, this.onError.bind(this));
        this.nodeIpcInstance.on(EventName.CONNECT, this.onConnect.bind(this));
        this.nodeIpcInstance.on(EventName.DISCONNECT, this.onDisconnect.bind(this));
        this.nodeIpcInstance.on(EventName.DESTROY, this.onDestroy.bind(this));
        this.nodeIpcInstance.on(EventName.SOCKET_DISCONNECTED, this.onSocketDisconnected.bind(this));
        this.nodeIpcInstance.on(EventName.DATA, this.onData.bind(this));
        this.nodeIpcInstance.on(CustomEventName.HELLO, this.onHello.bind(this));
        this.nodeIpcInstance.on(CustomEventName.SERVER_TERMINATES, this.onServerTerminates.bind(this));
    }

    protected unregisterEventListeners(): void {
        this.nodeIpcInstance.off(EventName.ERROR, this.onError.bind(this));
        this.nodeIpcInstance.off(EventName.CONNECT, this.onConnect.bind(this));
        this.nodeIpcInstance.off(EventName.DISCONNECT, this.onDisconnect.bind(this));
        this.nodeIpcInstance.off(EventName.DESTROY, this.onDestroy.bind(this));
        this.nodeIpcInstance.off(EventName.SOCKET_DISCONNECTED, this.onSocketDisconnected.bind(this));
        this.nodeIpcInstance.off(EventName.DATA, this.onData.bind(this));
        this.nodeIpcInstance.off(CustomEventName.HELLO, this.onHello.bind(this));
        this.nodeIpcInstance.off(CustomEventName.SERVER_TERMINATES, this.onServerTerminates.bind(this));
    }

    protected onError(error: any): void {
        if (this.state === State.STARTING || this.state === State.STOPPING) return this.reject(error);
        this.onUnknownError(error);
    }

    protected onConnect(socket?: Socket): void {
        this.onEvent(EventName.CONNECT, ...arguments);
    }

    protected onDisconnect(socket?: Socket): void {
        this.onEvent(EventName.DISCONNECT, ...arguments);
    }

    protected onDestroy(socket?: Socket): void {
        this.onEvent(EventName.DESTROY, ...arguments);
    }

    protected onSocketDisconnected(socket: Socket): void {
        this.onEvent(EventName.SOCKET_DISCONNECTED, ...arguments);
    }

    protected onData(data: Buffer): void {
        this.onEvent(EventName.DATA, ...arguments);
    }

    protected onHello(senderId: string, socket?: Socket): void {
        this.onEvent(CustomEventName.HELLO, ...arguments);
    }

    protected onServerTerminates(senderId: string, socket?: Socket): void {
        this.onEvent(CustomEventName.SERVER_TERMINATES, ...arguments);
    }

    protected onEvent(event: string, ...data: any[]) {
        // this.logger.log('Core.onEvent', event, this.id, ...data);
        this.logger.log("Core.onEvent", event, this.id);
    }

    private onUnknownError(error?: any) {
        if (error.code === "EPIPE" || error.message?.startsWith("unknown_clientid")) {
            this.logger.warn("Core.onUnknownError", this.id, error);
            return;
        }
        throw new Error(error?.toString());
    }

    private run(runFn: () => void): Promise<void> {
        return new Promise<void>((resolve: ResolveFn, reject: RejectFn) => {
            this._resolve = resolve;
            this._reject = reject;
            return runFn();
        });
    }
}
