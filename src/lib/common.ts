import { Socket } from "net";
import nodeIpc from "node-ipc";

type TypeOfNodeIpc = typeof nodeIpc;
export const NodeIpcStatic: TypeOfNodeIpc = nodeIpc;

export interface Config {
    debug?: boolean;
}

export const DefaultConfig: Config = {
    debug: true,
};

export enum EventName {
    ERROR = "error",
    CONNECT = "connect",
    DISCONNECT = "disconnect",
    DESTROY = "destroy",
    SOCKET_DISCONNECTED = "socket.disconnected",
    DATA = "data",
    START = "start",
}

export enum CustomEventName {
    HELLO = "HELLO",
    SERVER_TERMINATES = "SERVER_TERMINATES",
}

export const ERROR_INSTANCE_LOCKED: string = "Only one instance of IPC per process allowed";
export const ERROR_SAME_ID: string = "Client IPC must have another id than server";
export const ERROR_STATE_IS_NOT_STARTED: string = "IPC is not started";
export const ERROR_STATE_IS_NOT_STOPPED: string = "IPC is not stopped";

type SocketOr<Cond extends NodeIpcClientOrServer, Other> = Cond extends NodeIpcServer ? Socket : Other;
export type SocketOrNever<Cond extends NodeIpcClientOrServer> = SocketOr<Cond, never>;

type ErrorCb = (err: any) => void;
type SocketDisconnectedCb = (socket: Socket, destroyedSocketID: string) => void;
type DataCb = (data: Buffer) => void;
type EmptyCb = () => void;
type SocketOrEmptyCb<Cond extends NodeIpcClientOrServer> = (socket: SocketOrNever<Cond>) => void;
type CustomEventCb<Cond extends NodeIpcClientOrServer, Data> = Cond extends NodeIpcServer
    ? (data: Data, socket: Socket) => void
    : (data?: Data) => void;

export type NodeIpcServer = typeof NodeIpcStatic.server & {
    on(event: EventName.START, cb: EmptyCb): void;
    on<T>(event: string | CustomEventName, cb: CustomEventCb<NodeIpcClient, T>): void;
    off(event: EventName.START, cb: EmptyCb): void;
    off<T>(event: string | CustomEventName, cb: CustomEventCb<NodeIpcClient, T>): void;
    sockets: Socket[];
};
export type NodeIpcClient = {
    on(event: EventName.ERROR, cb: ErrorCb): void;
    on(event: EventName.CONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.DISCONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.DESTROY, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.SOCKET_DISCONNECTED, cb: SocketDisconnectedCb): void;
    on(event: EventName.DATA, cb: DataCb): void;
    on<T>(event: string | CustomEventName, cb: CustomEventCb<NodeIpcClient, T>): void;
    off(event: EventName.ERROR, cb: ErrorCb): NodeIpcClient;
    off(event: EventName.CONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.DISCONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.DESTROY, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.SOCKET_DISCONNECTED, cb: SocketDisconnectedCb): void;
    off(event: EventName.DATA, cb: DataCb): void;
    off<T>(event: string | CustomEventName, cb: CustomEventCb<NodeIpcClient, T>): void;
    emit(event: string, value?: any): void;
};
export type NodeIpcClientOrServer = NodeIpcClient | NodeIpcServer;

export type ResolveFn = () => void;
export type RejectFn = (reason?: any) => void;

export enum State {
    STOPPED = "STOPPED",
    STARTED = "STARTED",
    STARTING = "STARTING",
    STOPPING = "STOPPING",
}
