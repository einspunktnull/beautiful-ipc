import nodeIpc from "node-ipc";
import { Core } from "./Core";
import { Config, CustomEventName, ERROR_SAME_ID, EventName, NodeIpcClient, State } from "./common";
import assert from "assert";
import { Socket } from "net";

export class Client extends Core<NodeIpcClient> {
    public static async start(serverId: string, id: string, config?: Config): Promise<Client> {
        return new Client(serverId, id, config).start();
    }

    private readonly _serverId: string;

    public constructor(serverId: string, id: string, config?: Config) {
        super(id, config);
        assert(serverId !== id, ERROR_SAME_ID);
        this._serverId = serverId;
    }

    public get serverId(): string {
        return this._serverId;
    }

    protected initialize(): void {
        this.logger.log("Client.initialize", this.id);
        nodeIpc.connectTo(this.serverId, () => {
            this.initializeNodeIpcInstance(nodeIpc.of[this.serverId]);
        });
    }

    protected terminate(): void {
        this.logger.log("Client.terminate", this.id);
        nodeIpc.disconnect(this.serverId);
    }

    protected onConnect(): void {
        this.logger.log("Client.onConnect", this.id);
        this.sendToServer(CustomEventName.HELLO, this.id);
        this.resolve();
    }

    protected onDisconnect(socket: Socket): void {
        this.logger.log("Client.onDisconnect", this.id);
    }

    protected onServerTerminates(senderId: string, socket?: Socket): void {
        this.logger.log("Client.onServerTerminates", this.id);
    }

    private sendToServer(event: string, data?: any): void {
        this.nodeIpcInstance.emit(event, data);
    }
}
