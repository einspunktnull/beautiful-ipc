import { Core } from "./Core";
import { Config, CustomEventName, EventName, NodeIpcServer, NodeIpcStatic, State } from "./common";
import { Socket } from "net";

export class Server extends Core<NodeIpcServer> {
    public static async start(id: string, config?: Config): Promise<Server> {
        return new Server(id, config).start();
    }

    private readonly registeredSockets: Map<string, Socket> = new Map<string, Socket>();

    public constructor(id: string, config?: Config) {
        super(id, config);
    }

    protected initialize(): void {
        // this.logger.log('Server.initialize', this.id);
        NodeIpcStatic.serve();
        this.initializeNodeIpcInstance(<NodeIpcServer>NodeIpcStatic.server);
        this.nodeIpcInstance.start();
    }

    protected terminate(): void {
        this.logger.log("Server.terminate", this.id);
        this.nodeIpcInstance.stop();
        this.sendToAllClients(CustomEventName.SERVER_TERMINATES);
        this.finalizeNodeIpcInstance();
        this.resolve();
    }

    protected registerEventListeners(): void {
        this.nodeIpcInstance.on(EventName.START, this.onStart.bind(this));
        super.registerEventListeners();
    }

    protected unregisterEventListeners(): void {
        this.nodeIpcInstance.off(EventName.START, this.onStart.bind(this));
        super.unregisterEventListeners();
    }

    protected onStart(): void {
        this.logger.log("Server.onStart", this.id);
        this.resolve();
    }

    protected onHello(clientId: string, socket: Socket): void {
        // this.logger.log('Server.onHello', this.id, clientId);
        if (this.registeredSockets.has(clientId)) return;
        this.registeredSockets.set(clientId, socket);
        this.sendToClient(socket, CustomEventName.HELLO, this.id);
    }

    protected onConnect(socket: Socket): void {
        this.logger.log("Server.onConnect", this.id);
    }

    protected onDisconnect(socket: Socket): void {
        this.logger.log("Server.onDisconnect", this.id);
    }

    protected onSocketDisconnected(socket: Socket): void {
        for (const [key, regSocket] of this.registeredSockets) {
            if (regSocket === socket) {
                this.registeredSockets.delete(key);
                return;
            }
        }
    }

    private sendToAllClients(event: string, data?: any): void {
        this.nodeIpcInstance.sockets.forEach((socket: Socket) => {
            this.sendToClient(socket, event, data);
        });
    }

    private sendToClient(clientIdOrSocket: string | Socket, event: string, data?: any): void {
        if (clientIdOrSocket instanceof Socket) {
            this.nodeIpcInstance.emit(clientIdOrSocket, event, data);
            return;
        }
        const socket: Socket = this.registeredSockets.get(clientIdOrSocket);
        if (!socket) return this.onError(new Error("unknown_clientid: " + clientIdOrSocket));
        this.nodeIpcInstance.emit(socket, event, data);
    }
}
