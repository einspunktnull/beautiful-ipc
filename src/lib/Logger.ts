export class Logger {
    private readonly debug: boolean;

    public constructor(debug: boolean) {
        this.debug = debug;
    }

    public log(...args: any[]): void {
        if (!this.debug) return;
        console.log(...args);
    }

    public error(...args: any[]): void {
        if (!this.debug) return;
        console.error(...args);
    }

    public warn(...args: any[]): void {
        if (!this.debug) return;
        console.warn(...args);
    }
}
